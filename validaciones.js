const name = document.getElementById("txtName");
const lastName = document.getElementById("txtLastName");
const email = document.getElementById("txtEmail");
const pass = document.getElementById("txtPass");
const passVal = document.getElementById("txtPassVal");
const number = document.getElementById("txtPhone");
const age = document.getElementById("txtAge");
const body = document.getElementById("main");
const secret = document.getElementById("konamiSec");
const konami = ["arrowup","arrowdown","arrowup","arrowdown","arrowleft","arrowright","arrowleft","arrowright","b","a","enter"];
const form = document.getElementById("form1");
let codigo=0;


document.addEventListener('keydown', (e) => {
  const keyName = e.key.toLowerCase();
  console.log(keyName);
  if(konami[codigo] == keyName){
    codigo++;
  }else{
    codigo = 0;
  }
  console.log(codigo);
  
  if(codigo == 11){
    body.hidden=true;
    secret.hidden = false;
  }

});

form.addEventListener("submit", (e) => {

  let erroMsg = [];
  if (name.value === "" || name.value == null){
    erroMsg.push("Name is required");
  }

  if(lastName.value === "" || name.value == null){
    erroMsg.push("Last Name is required");
  }

  if(email.value === "" || email.value == null){
    erroMsg.push("Email is required");
  }

  if(pass.value === "" || pass == null){
    erroMsg.push("Last Name is required");
  }else if(pass.length < 1 || pass.length > 10){
    erroMsg.push("Password most between 4 and 10 characters");
  }

  if(passVal.value === "" || passVal == null){
    erroMsg.push("Last Name is required");
  }else if(passVal.value == pass.value){
    erroMsg.push("The password validation need to the same as the password field");
  }

  if(number.value === "" || number == null){
    erroMsg.push("Phone Number is required");
  }else if(number.length < 8 || number.length > 13){
    erroMsg.push("Type a valied Phone Number");
  }

  if(age.value=== "" || age == null){
    erroMsg.push("Age is required");
  }else if(age.value < 18){
    erroMsg.push("Most be older than 18 to register");
  } else if(age.value > 90){
    erroMsg.push("type a valid age");
  }

  if(erroMsg.length > 0) {
    e.preventDefault();
    alert(erroMsg.join(", "));
  }else{
    alert("Succes")
  }

})